var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var express = require('express');
var expressSession = require('express-session');
var path = require('path');
var wvs = require('../whitefoxx-vendors-sync');

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cookieParser());
app.use(express.static(__dirname + '/public'));
app.use(expressSession({
    resave: true,
    saveUninitialized: true,
    secret: 'secret'
}));

app.listen(3000, function () {
    console.log('LISTENING ON PORT 3000');
});

app.get('/', function (req, res) {
    res.render('shopfront');
});

app.get('/shipping-address', function (req, res) {
    res.render('shipping-address');
});

app.get('/select-shipping', function (req, res) {
    res.render('select-shipping');
});

app.get('/checkout', function (req, res) {
    res.render('checkout');
});
